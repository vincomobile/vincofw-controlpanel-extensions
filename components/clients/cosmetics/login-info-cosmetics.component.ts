import {Component, Input, OnInit} from '@angular/core';
import {VincoComponent} from '../../../../vincofw/models/component-register.model';

@Component({
    selector: 'custom-login-info-cosmetics',
    templateUrl: './login-info-cosmetics.component.html',
    styleUrls: ['./login-info-cosmetics.component.scss']
})
export class LoginInfoCosmeticsComponent implements VincoComponent, OnInit {

    @Input() data: any;

    constructor() {
    }

    ngOnInit(): void {
    }

}
