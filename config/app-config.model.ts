import {BaseSettingsModel} from '../../vincofw/models/base-settings.models';
import {environment} from '../../../environments/environment';

/**
 * Default application configuration
 */
export const APP_CONFIG: BaseSettingsModel = {
    REST_HOST_PATH: environment.baseUrl + '/api/',
    REST_HOST_CORE: environment.baseUrl + '/api/vinco_core/',
    REST_HOST_LOGIN: environment.baseUrl + '/api/vinco_core/',
    ID_CLIENT: 'ff8081815cce8ef3015cce91f2d400cd',
    ID_LANGUAGE: 'D804EFC2B38FEEA39FA751540709D0BA', // Spanish,
    COUNTRY_CODE: 'es',
    LANGUAGE: 'es', // Spanish,
    APPLICATION_LOGO_URL: 'app/extensions/assets/images/logo_vinco.png',
    LOGO_CLIENT_PATH: 'logo_client.png',
    APPLICATION_ICON_URL: 'favicon.ico',
    APP_NAME: 'DEMO_A11',
    CLIENT_FULL_NAME: 'VincoMobile E-Catalog DEMO',
    SECRET_KEY: 'DEMO_16dcfa36-46e2-11eb-85bc-b42e990f5de8',
    TABLE_PREFIX_VERSIONS: [],
    SHOW_MENU_FILTER: true
};

export const DATE_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMM YYYY',
    }
};

export const DATE_LOCALE = 'es-ES';
