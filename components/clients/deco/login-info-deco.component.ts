import {Component, Input, OnInit} from '@angular/core';
import {VincoComponent} from '../../../../vincofw/models/component-register.model';

@Component({
    selector: 'custom-login-info-deco',
    templateUrl: './login-info-deco.component.html',
    styleUrls: ['./login-info-deco.component.scss']
})
export class LoginInfoDecoComponent implements VincoComponent, OnInit {

    @Input() data: any;

    constructor() {
    }

    ngOnInit(): void {
    }

}
