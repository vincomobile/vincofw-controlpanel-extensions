import {NgModule} from '@angular/core';
import {UiUxModule} from '../ui-ux/ui-ux.module';
import {LoginInfoCosmeticsComponent} from './components/clients/cosmetics/login-info-cosmetics.component';
import {LoginInfoDecoComponent} from './components/clients/deco/login-info-deco.component';
import {LoginInfoDefaultComponent} from './components/clients/default/login-info-default.component';
import {LoginInfoRetailComponent} from './components/clients/retail/login-info-retail.component';
import {VincofwModule} from "../vincofw/vincofw.module";

@NgModule({
    declarations: [
        LoginInfoCosmeticsComponent,
        LoginInfoDecoComponent,
        LoginInfoDefaultComponent,
        LoginInfoRetailComponent
    ],
    entryComponents: [
        LoginInfoCosmeticsComponent,
        LoginInfoDecoComponent,
        LoginInfoDefaultComponent,
        LoginInfoRetailComponent
    ],
    imports: [
        UiUxModule,
        VincofwModule
    ],
    providers: [

    ],
    exports: [

    ]
})

export class ExtensionsModule {
}
