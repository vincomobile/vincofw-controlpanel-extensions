import {Injectable} from '@angular/core';
import {AppLoadInterface} from '../../vincofw/services/interface.service';
import {ComponentsService} from '../../vincofw/services/components.service';
import {ComponentCategory, ComponentModel} from '../../vincofw/models/component-register.model';
import {LoginInfoDecoComponent} from '../components/clients/deco/login-info-deco.component';
import {LoginInfoCosmeticsComponent} from '../components/clients/cosmetics/login-info-cosmetics.component';
import {LoginInfoRetailComponent} from '../components/clients/retail/login-info-retail.component';
import {LoginInfoDefaultComponent} from '../components/clients/default/login-info-default.component';

@Injectable({
    providedIn: 'root'
})
export class CustomLoadService implements AppLoadInterface {

    constructor(
        private _componentsService: ComponentsService) {
    }

    /**
     * Initial application load
     *
     * @param data Data load in previous load services
     */
    load(data: any): Promise<any> {
        return new Promise((resolve, reject) => {
            console.log(`CustomLoadService.load:: Custom app initialization`);
            if (data) {
                switch (data.idClient) {
                    // Vincomobile DEMO (Retail)
                    case 'ff80818172a85e090172a91edaa7000a':
                        this._componentsService.setComponent(ComponentCategory.LoginInfo, new ComponentModel(LoginInfoRetailComponent, {}));
                        break;

                    // Vincomobile DEMO (Furniture & Decorations)
                    case 'ff8081815cce8ef3015cce91f2d400cd':
                        this._componentsService.setComponent(ComponentCategory.LoginInfo, new ComponentModel(LoginInfoDecoComponent, {}));
                        break;

                    // Vincomobile DEMO (Cosmetics)
                    case 'ff8081815ccfb362015ccfb6fef60001':
                        this._componentsService.setComponent(ComponentCategory.LoginInfo, new ComponentModel(LoginInfoCosmeticsComponent, {}));
                        break;

                    default:
                        this._componentsService.setComponent(ComponentCategory.LoginInfo, new ComponentModel(LoginInfoDefaultComponent, {}));
                }
            } else {
                this._componentsService.setComponent(ComponentCategory.LoginInfo, new ComponentModel(LoginInfoDefaultComponent, {}));
            }
            // TODO: Remove timeout (is only for testing load page)
            setTimeout(() => {
                resolve(data);
            }, 500);
        });
    }

    /**
     * Add Font Awesome Icons to library
     */
    addFontAwesomeIcons(): void {

    }

}
