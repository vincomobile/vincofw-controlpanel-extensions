import {Component, Input, OnInit} from '@angular/core';
import {VincoComponent} from '../../../../vincofw/models/component-register.model';

@Component({
    selector: 'custom-login-info-default',
    templateUrl: './login-info-default.component.html',
    styleUrls: ['./login-info-default.component.scss']
})
export class LoginInfoDefaultComponent implements VincoComponent, OnInit {

    @Input() data: any;

    constructor() {
    }

    ngOnInit(): void {
    }

}
