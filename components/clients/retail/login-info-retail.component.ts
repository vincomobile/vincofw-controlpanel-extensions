import {Component, Input, OnInit} from '@angular/core';
import {VincoComponent} from '../../../../vincofw/models/component-register.model';

@Component({
    selector: 'custom-login-info-retail',
    templateUrl: './login-info-retail.component.html',
    styleUrls: ['./login-info-retail.component.scss']
})
export class LoginInfoRetailComponent implements VincoComponent, OnInit {

    @Input() data: any;

    constructor() {
    }

    ngOnInit(): void {
    }

}
